package com.report.extractor.service;

import com.report.extractor.db.DatabaseManager;
import com.report.extractor.db.Repository;
import com.report.extractor.db.ResultData;
import com.report.extractor.email.EmailService;
import com.report.extractor.encrtyption.FileEncryptor;
import com.report.extractor.files.CsvWriter;
import com.report.extractor.props.ReportExtractorNonRepeatableProperties;
import com.report.extractor.props.ReportExtractorProperties;
import com.report.extractor.props.ReportExtractorRepeatableProperties;
import com.report.extractor.utils.Utils;
import lombok.extern.log4j.Log4j2;

import javax.mail.MessagingException;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

@Log4j2
public class ReportExtractorService {

    private static final String CSV_FILE_EXTENSION = "csv";
    private static final String GPG_FILE_EXTENSION = "gpg";

    private final EmailService emailService;

    public ReportExtractorService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void handle(ReportExtractorProperties reportExtractorProperties) {

        ReportExtractorNonRepeatableProperties nonRepeatableProperties = reportExtractorProperties.getNonRepeatableProperties();

        reportExtractorProperties.getRepeatableProperties().forEach(repeatableProperties -> {
            try {
                log.info("Creating report fot the group [ {} ]", repeatableProperties.getGroupName());
                DatabaseManager databaseManager = new DatabaseManager(repeatableProperties);
                Repository repository = new Repository(databaseManager);
                ResultData resultData = repository.select(repeatableProperties.getDatasetQuery());
                String outputFile = writeFile(repeatableProperties, nonRepeatableProperties, resultData);
                sendEmail(repeatableProperties, nonRepeatableProperties, outputFile);
                log.info("Created report fot the group [ {} ] \n", repeatableProperties.getGroupName());
            } catch (Exception e) {
                log.error("{}", e.getMessage());
                Optional.ofNullable(e.getCause()).ifPresent(message -> log.error("Cause: {}", message));
            }
        });
    }

    private static String writeFile(ReportExtractorRepeatableProperties repeatableProperties,
            ReportExtractorNonRepeatableProperties nonRepeatableProperties, ResultData resultData) throws Exception {
        String fileExtension = getFileExtension(nonRepeatableProperties);
        String csvPath = Utils.generateOutputFilePath(repeatableProperties.getOutputFolder(),
                repeatableProperties.getOutputFileNamePrefix(), fileExtension);
        log.info("Output file path: {}", csvPath);
        log.debug("Output file using gpg encryption? {}", nonRepeatableProperties.getGpgEncryptionEnabled());
        String outputFilePath = csvPath;
        if (nonRepeatableProperties.getGpgEncryptionEnabled()) {
            File temp = File.createTempFile(Utils.generateFileName(repeatableProperties.getOutputFileNamePrefix()), fileExtension);
            outputFilePath = temp.getAbsolutePath();
        }

        CsvWriter csvWriter = new CsvWriter(outputFilePath, nonRepeatableProperties.getIncludeColumnHeaders(),
                nonRepeatableProperties.getUseTextQualifiers());
        csvWriter.write(resultData);

        if (nonRepeatableProperties.getGpgEncryptionEnabled()) {
            FileEncryptor.encrypt(nonRepeatableProperties.getGpgPublicKeyPath(), nonRepeatableProperties.getGpgPrivateKeyPath(),
                    outputFilePath, csvPath);
            log.info("Stored encrypted file: [ {} ]", csvPath);
            if (Paths.get(outputFilePath).toFile().exists())
                Files.delete(Paths.get(outputFilePath));
        } else {
            log.info("Stored plain file: [ {}] ", outputFilePath);
        }
        return nonRepeatableProperties.getGpgEncryptionEnabled() ? csvPath : outputFilePath;
    }

    private void sendEmail(ReportExtractorRepeatableProperties repeatableProperties,
            ReportExtractorNonRepeatableProperties nonRepeatableProperties, String outputFile) throws MessagingException {
        log.debug("Send email? {}", repeatableProperties.getEmailDistributionEnabled());
        if (repeatableProperties.getEmailDistributionEnabled())
            emailService.sendEmail(repeatableProperties, nonRepeatableProperties, outputFile);
    }

    private static String getFileExtension(ReportExtractorNonRepeatableProperties nonRepeatableProperties) {
        return nonRepeatableProperties.getGpgEncryptionEnabled() ? GPG_FILE_EXTENSION : CSV_FILE_EXTENSION;
    }
}
