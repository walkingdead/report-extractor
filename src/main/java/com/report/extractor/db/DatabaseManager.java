package com.report.extractor.db;

import com.report.extractor.ReportExtractorException;
import com.report.extractor.props.ReportExtractorRepeatableProperties;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@Log4j2
public class DatabaseManager {

    private final ReportExtractorRepeatableProperties extractGpgConfiguration;

    static {
        String driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        try {
            log.debug("Loaded {}", driverClassName);
            Class.forName(driverClassName);
            log.debug("Successfully loaded {}", driverClassName);
        } catch (ClassNotFoundException e) {
            throw new ReportExtractorException(driverClassName + " not found", e);
        }
    }

    public DatabaseManager(ReportExtractorRepeatableProperties extractGpgConfiguration) {
        this.extractGpgConfiguration = extractGpgConfiguration;
    }

    public Connection getDatabaseConnection() throws SQLException {
        String connectionUrl = String
                .format("jdbc:sqlserver://%s:1433;databaseName=%s", extractGpgConfiguration.getDatabaseHost(), extractGpgConfiguration.getDatabaseName());
        log.debug("Database connection url [ {} ]", connectionUrl);
        Properties props = new Properties();
        props.setProperty("user", extractGpgConfiguration.getDatabaseUser());
        props.setProperty("password", extractGpgConfiguration.getDatabasePassword());
        return DriverManager.getConnection(connectionUrl, props);
    }
}