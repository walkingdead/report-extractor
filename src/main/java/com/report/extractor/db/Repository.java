package com.report.extractor.db;

import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class Repository {

    private final DatabaseManager databaseManager;

    public Repository(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public ResultData select(String sql) throws Exception {
        log.info("Extracting data using query [{}]", sql);
        Connection conn = databaseManager.getDatabaseConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        ResultSetMetaData rsmd = rs.getMetaData();
        List<List<String>> columnValues = new ArrayList<>();
        List<String> columnNames = new ArrayList<>();
        int columnsNumber = rsmd.getColumnCount();

        log.debug("Total columns number [{}]", columnsNumber);
        for (int i = 1; i <= columnsNumber; i++) {
            columnNames.add(rs.getMetaData().getColumnLabel(i));
        }

        while (rs.next()) {
            List<String> row = new ArrayList<>();
            for (int i = 1; i <= columnsNumber; i++) {
                row.add(String.valueOf(rs.getObject(i)));
            }
            columnValues.add(row);
        }
        log.debug("Total rows extracted [{}]", columnValues.size());
        stmt.close();
        conn.close();
        return ResultData.builder().columnNames(columnNames).columnValues(columnValues).build();
    }
}
