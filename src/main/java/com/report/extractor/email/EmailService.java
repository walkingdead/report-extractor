package com.report.extractor.email;

import com.report.extractor.props.ReportExtractorNonRepeatableProperties;
import com.report.extractor.props.ReportExtractorRepeatableProperties;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

@Log4j2
public class EmailService {

    public void sendEmail(ReportExtractorRepeatableProperties repeatableProperties,
            ReportExtractorNonRepeatableProperties nonRepeatableProperties, String outputFile) throws MessagingException {
        Session mailSession = getMailSession(nonRepeatableProperties);
        Message emailMessage = getEmailMessage(repeatableProperties, nonRepeatableProperties, mailSession);
        Multipart attachment = buildAttachment(outputFile, repeatableProperties);
        emailMessage.setContent(attachment);
        Transport.send(emailMessage);
        log.info("Email to [{}] was successfully send", repeatableProperties.getEmailAddressTo());
    }

    private static Session getMailSession(ReportExtractorNonRepeatableProperties nonRepeatableProperties) {
        Properties emailProperties = System.getProperties();
        emailProperties.put("mail.smtp.port", nonRepeatableProperties.getSmtpHostPort());
        emailProperties.put("mail.smtp.host", nonRepeatableProperties.getSmtpHostName());
        log.debug("Email smpt.port={} smtp.host={}", nonRepeatableProperties.getSmtpHostPort(), nonRepeatableProperties.getSmtpHostPort());
        appendAuthIfApplicable(emailProperties, nonRepeatableProperties);
        return Session.getDefaultInstance(emailProperties, getAuthenticator(nonRepeatableProperties));
    }

    private MimeMessage getEmailMessage(ReportExtractorRepeatableProperties repeatableProperties,
            ReportExtractorNonRepeatableProperties nonRepeatableProperties,
            Session mailSession) throws MessagingException {

        String toEmail = repeatableProperties.getEmailAddressTo();
        String emailSubject = repeatableProperties.getEmailSubject();

        log.info("Sending email from [ {} ] to [ {} ]", nonRepeatableProperties.getEmailAddressFrom(), toEmail);

        MimeMessage emailMessage = new MimeMessage(mailSession);

        emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));

        emailMessage.setSubject(emailSubject);
        emailMessage.setFrom(nonRepeatableProperties.getEmailAddressFrom());
        return emailMessage;
    }

    private Multipart buildAttachment(String filePath, ReportExtractorRepeatableProperties repeatableProperties) throws MessagingException {

        Multipart multipart = new MimeMultipart();

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(repeatableProperties.getEmailBody());
        multipart.addBodyPart(messageBodyPart);


        messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(repeatableProperties.getEmailBody());
        String fileName = FilenameUtils.getName(filePath);
        DataSource source = new FileDataSource(filePath);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(fileName);
        multipart.addBodyPart(messageBodyPart);

        return multipart;
    }

    private static Authenticator getAuthenticator(ReportExtractorNonRepeatableProperties nonRepeatableProperties) {

        if (!isTestUserPasswordProvided(nonRepeatableProperties)) {
            return null;
        }
        log.debug("Using username and password for testing email");
        return new Authenticator() {

            @Override protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(nonRepeatableProperties.getSmtpHostUser(), nonRepeatableProperties.getSmtpHostPassword());
            }
        };
    }

    private static boolean isTestUserPasswordProvided(ReportExtractorNonRepeatableProperties nonRepeatableProperties) {
        return StringUtils.isNotBlank(nonRepeatableProperties.getSmtpHostUser()) && StringUtils.isNotBlank(nonRepeatableProperties.getSmtpHostPassword());
    }

    private static void appendAuthIfApplicable(Properties emailProperties, ReportExtractorNonRepeatableProperties nonRepeatableProperties) {
        if (isTestUserPasswordProvided(nonRepeatableProperties)) {
            log.debug("Setting auth email properties for testing purposes only");
            emailProperties.put("mail.smtp.auth", "true");
            emailProperties.put("mail.smtp.starttls.enable", "true");
        }
    }
}
