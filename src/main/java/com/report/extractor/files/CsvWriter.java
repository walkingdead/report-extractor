package com.report.extractor.files;

import com.report.extractor.db.ResultData;
import com.opencsv.CSVWriter;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@AllArgsConstructor
@Log4j2
public class CsvWriter {

    private final String outputFilePath;
    private final boolean includeHeader;
    private final boolean useTextQualifiers;

    public void write(ResultData resultData) throws IOException {
        log.debug("Include header? {}", includeHeader);
        log.debug("Quote character? {}", useTextQualifiers);
        try (
                Writer writer = Files.newBufferedWriter(Paths.get(outputFilePath));
                CSVWriter csvWriter = new CSVWriter(writer,
                        CSVWriter.DEFAULT_SEPARATOR,
                        useTextQualifiers ? CSVWriter.DEFAULT_QUOTE_CHARACTER : CSVWriter.NO_QUOTE_CHARACTER,
                        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
        ) {
            if (includeHeader)
                csvWriter.writeNext(resultData.getColumnNames().toArray(new String[0]));
            resultData.getColumnValues().forEach((List<String> row) -> csvWriter.writeNext(row.toArray(new String[0])));
        }
    }
}
