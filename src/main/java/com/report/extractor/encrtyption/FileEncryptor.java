package com.report.extractor.encrtyption;

import lombok.experimental.UtilityClass;

import java.security.Security;

@UtilityClass
public class FileEncryptor {

    public static void encrypt(String publicSignKeyPath, String privateSignKeyPath,  String inputFilePath, String encryptedFilePath) throws Exception {
        BCPGPEncryptor encryptor = new BCPGPEncryptor();
        encryptor.setArmored(false);
        encryptor.setCheckIntegrity(true);
        encryptor.setPublicKeyFilePath(publicSignKeyPath);
        encryptor.setSigning(true);
        encryptor.setSigningPrivateKeyFilePath(privateSignKeyPath);
        encryptor.setSigningPrivateKeyPassword("");
        encryptor.encryptFile(inputFilePath, encryptedFilePath);
    }

    public static void decryptFile() throws Exception {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        BCPGPDecryptor decryptor = new BCPGPDecryptor();
        decryptor.setPrivateKeyFilePath("/Users/konstantin/settings/sally_gpg_private.key");
        decryptor.setSigningPublicKeyFilePath("/Users/konstantin/settings/sally_gpg_public.key");
        decryptor.setSigned(true);
        decryptor.setPassword("");
        decryptor.decryptFile("/Users/konstantin/settings/out/CARS-2019-28-15-102843810.gpg",
                "/Users/konstantin/settings/out/decoded.csv");
    }

    public static void main(String[] args) throws Exception {
        FileEncryptor.decryptFile();
    }

}
