package com.report.extractor;

import com.report.extractor.email.EmailService;
import com.report.extractor.props.ReportExtractorProperties;
import com.report.extractor.props.utils.PropertiesReader;
import com.report.extractor.service.ReportExtractorService;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;

import java.security.Security;
import java.util.Optional;

@Log4j2
public class ReportExtractor {

    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        setLogLevel();
    }

    public static void main(String[] args) {
        log.info("Starting Report Extractor...");

        String propertyPath = System.getProperty("property-path");
        log.info("Configuration property file [{}] \n", propertyPath);

        try {
            ReportExtractorProperties reportExtractorProperties = PropertiesReader.readProperties(propertyPath);
            EmailService emailService = new EmailService();
            ReportExtractorService reportExtractorService = new ReportExtractorService(emailService);
            reportExtractorService.handle(reportExtractorProperties);
        } catch (ReportExtractorException ree) {
            log.error("ReportExtractorException. {}", ree.getMessage());
            Optional.ofNullable(ree.getCause()).ifPresent(message -> log.error("Cause: {}",message));
        } catch (Exception e) {
            log.error("Exception. {}", e.getMessage());
            Optional.ofNullable(e.getCause()).ifPresent(message -> log.error("Exception ReportExtractorException Cause: {}",message));
        }
        log.info("Report Extractor completed");
    }

    private static void setLogLevel() {
        String logLevel = System.getProperty("log4j.level", "INFO");
        log.info("Setting log level [ {} ]", logLevel);
        Configurator.setLevel(System.getProperty("log4j.logger"), Level.getLevel(logLevel));
    }
}
