package com.report.extractor.props;

import com.report.extractor.props.utils.PropertiesReader;
import com.report.extractor.props.utils.PropertyField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReportExtractorNonRepeatableProperties {

    @PropertyField(name = "gpg.encryption.enabled", filedMapper = PropertiesReader.FieldMapper.BOOLEAN)
    private Boolean gpgEncryptionEnabled;

    @PropertyField(name = "gpg.public.key.path")
    private String gpgPublicKeyPath;

    @PropertyField(name = "gpg.private.key.path")
    private String gpgPrivateKeyPath;

    @PropertyField(name = "use.text.qualifiers", filedMapper = PropertiesReader.FieldMapper.BOOLEAN)
    private Boolean useTextQualifiers;

    @PropertyField(name = "include.column.headers", filedMapper = PropertiesReader.FieldMapper.BOOLEAN)
    private Boolean includeColumnHeaders;

    @PropertyField(name = "email.address.from")
    private String emailAddressFrom;

    @PropertyField(name = "smtp.host.name")
    private String smtpHostName;

    @PropertyField(name = "smtp.host.port")
    private String smtpHostPort;

    @PropertyField(name = "smtp.host.user")
    private String smtpHostUser;

    @PropertyField(name = "smtp.host.pass")
    private String smtpHostPassword;
}
