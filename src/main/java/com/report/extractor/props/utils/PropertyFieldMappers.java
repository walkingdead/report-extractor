package com.report.extractor.props.utils;

import lombok.experimental.UtilityClass;

import java.util.Map;
import java.util.function.BiFunction;

@UtilityClass final class PropertyFieldMappers {

    static final BiFunction<Map<Object, Object>, Object, String> MAP_STRING = (Map<Object, Object> attributes, Object key) -> (String) attributes
            .get(key);

    static final BiFunction<Map<Object, Object>, Object, Boolean> MAP_BOOLEAN = (Map<Object, Object> attributes, Object key) ->
            "Yes".equals((String) attributes.get(key));
}
