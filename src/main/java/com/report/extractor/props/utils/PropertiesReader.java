package com.report.extractor.props.utils;

import com.report.extractor.ReportExtractorException;
import com.report.extractor.props.ReportExtractorProperties;
import com.report.extractor.props.ReportExtractorRepeatableProperties;
import com.report.extractor.props.ReportExtractorNonRepeatableProperties;
import com.google.common.collect.ImmutableMap;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static com.report.extractor.props.utils.PropertiesReader.FieldMapper.BOOLEAN;
import static com.report.extractor.props.utils.PropertiesReader.FieldMapper.STRING;
import static com.report.extractor.props.utils.PropertyFieldMappers.MAP_BOOLEAN;
import static com.report.extractor.props.utils.PropertyFieldMappers.MAP_STRING;

@UtilityClass
public class PropertiesReader {

    public enum FieldMapper {
        STRING,
        BOOLEAN
    }

    private static final Map<FieldMapper, BiFunction<Map<Object, Object>, Object, ?>> FIELD_MAPPERS =
            ImmutableMap.<FieldMapper, BiFunction<Map<Object, Object>, Object, ?>>builder()
                    .put(STRING, MAP_STRING)
                    .put(BOOLEAN, MAP_BOOLEAN)
                    .build();

    public static ReportExtractorProperties readProperties(String filePath) {
        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream(filePath));
            Map<String, Map<Object, Object>> repeatableProperties = new HashMap<>();
            Map<Object, Object> nonRepeatableProperties = new HashMap<>();

            Enumeration enuKeys = prop.keys();
            while (enuKeys.hasMoreElements()) {
                String key = (String) enuKeys.nextElement();
                String value = prop.getProperty(key);
                String[] split = key.split("\\.");
                String setName = split[split.length - 1];
                nonRepeatableProperties.put(key, value);
                repeatableProperties.putIfAbsent(setName, new HashMap<>());
                repeatableProperties.get(setName).put(String.join(".", Arrays.copyOf(split, split.length - 1)), value);
            }
            List<ReportExtractorRepeatableProperties> repeatablePropertiesList = repeatableProperties.keySet().stream()
                    .map(key -> mapRepeatable(repeatableProperties.get(key), ReportExtractorRepeatableProperties.class, key))
                    .filter(v -> !ReportExtractorRepeatableProperties.buildWithGroupName(v.getGroupName()).equals(v))
                    .collect(Collectors.toList());

            ReportExtractorNonRepeatableProperties nonRepeatablePropertiesList = map(nonRepeatableProperties, ReportExtractorNonRepeatableProperties.class);

            return ReportExtractorProperties.builder().repeatableProperties(repeatablePropertiesList).nonRepeatableProperties(nonRepeatablePropertiesList)
                    .build();
        } catch (IOException e) {
            throw new ReportExtractorException("Failed to read property file" + filePath, e);
        }
    }

    private static <T> T mapRepeatable(Map<Object, Object> attributes, Class<T> entityClass, String groupName) {

        T entity;
        try {
            entity = entityClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new ReportExtractorException("Failed to instantiate " + entityClass, e);
        }
        Arrays.stream(entityClass.getDeclaredFields())
                .filter(field -> field.getAnnotation(PropertyField.class) != null && field.getAnnotation(PropertyField.class).repeatable())
                .forEach(field -> setFiled(attributes, field.getAnnotation(PropertyField.class), entity, field));
        //Sey groupName
        setFiled(entity, "groupName", groupName);
        return entity;
    }

    private static <T> T map(Map<Object, Object> attributes, Class<T> entityClass) {

        T entity;
        try {
            entity = entityClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new ReportExtractorException("Failed to instantiate " + entityClass, e);
        }
        Arrays.stream(entityClass.getDeclaredFields())
                .filter(field -> field.getAnnotation(PropertyField.class) != null && !field.getAnnotation(PropertyField.class).repeatable())
                .forEach(field -> setFiled(attributes, field.getAnnotation(PropertyField.class), entity, field));
        return entity;
    }

    private static <T> void setFiled(Map<Object, Object> attributes, PropertyField annotation, T entity, Field field) {
        String fieldName = annotation.name();
        FieldMapper fieldMapper = annotation.filedMapper();
        if (FIELD_MAPPERS.get(fieldMapper) == null) {
            throw new ReportExtractorException(String.format("Field mapper [%s] is not registered", fieldMapper));
        }
        try {
            FieldUtils.writeDeclaredField(entity, field.getName(), FIELD_MAPPERS.get(fieldMapper).apply(attributes, fieldName), true);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new ReportExtractorException(String.format("Failed to set field %s on Entity %s", field.getName(), entity.getClass().getName()), e);
        }
    }

    private static <T> void setFiled(T entity, String fieldName, String fieldValue) {
        try {
            FieldUtils.writeDeclaredField(entity, fieldName, fieldValue, true);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new ReportExtractorException(String.format("Failed to set field %s on Entity %s", fieldName, entity.getClass().getName()), e);
        }
    }
}
