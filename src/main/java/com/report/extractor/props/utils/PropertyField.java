package com.report.extractor.props.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface PropertyField {

    String name() default "";

    boolean repeatable() default false;

    PropertiesReader.FieldMapper filedMapper() default PropertiesReader.FieldMapper.STRING;
}
