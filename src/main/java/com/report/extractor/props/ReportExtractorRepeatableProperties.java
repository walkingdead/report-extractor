package com.report.extractor.props;

import com.report.extractor.props.utils.PropertiesReader;
import com.report.extractor.props.utils.PropertyField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReportExtractorRepeatableProperties {

    // Db settings
    @PropertyField(name = "database.host", repeatable = true)
    private String databaseHost;
    @PropertyField(name = "database.name", repeatable = true)
    private String databaseName;
    @PropertyField(name = "database.user", repeatable = true)
    private String databaseUser;
    @PropertyField(name = "database.password", repeatable = true)
    private String databasePassword;

    // Query
    @PropertyField(name = "dataset.query", repeatable = true)
    private String datasetQuery;

    // Output
    @PropertyField(name = "output.folder", repeatable = true)
    private String outputFolder;

    @PropertyField(name = "file.name.prefix", repeatable = true)
    private String outputFileNamePrefix;

    //Email
    @PropertyField(name = "email.distribution.enabled", repeatable = true, filedMapper = PropertiesReader.FieldMapper.BOOLEAN)
    private Boolean emailDistributionEnabled;

    @PropertyField(name = "email.address.to", repeatable = true)
    private String emailAddressTo;

    @PropertyField(name = "email.body", repeatable = true)
    private String emailBody;

    @PropertyField(name = "email.subject", repeatable = true)
    private String emailSubject;

    private String groupName;

    public static ReportExtractorRepeatableProperties buildWithGroupName(String groupName){
        return ReportExtractorRepeatableProperties.builder().groupName(groupName).emailDistributionEnabled(false).build();
    }
}
