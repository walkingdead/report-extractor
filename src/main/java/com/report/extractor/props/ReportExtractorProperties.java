package com.report.extractor.props;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReportExtractorProperties {

    private List<ReportExtractorRepeatableProperties> repeatableProperties;
    private ReportExtractorNonRepeatableProperties nonRepeatableProperties;
}
