package com.report.extractor.utils;

import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@UtilityClass
public class Utils {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd-hhmmssSSS");

    public static String generateOutputFilePath(String basePath, String prefix, String extension) {
        return String.format("%s/%s%s.%s", basePath, prefix,
                LocalDateTime.now().format(DATE_TIME_FORMATTER), extension);
    }

    public static String generateFileName(String prefix){
        return String.format("%s%s", prefix, LocalDateTime.now().format(DATE_TIME_FORMATTER));
    }
}
