package com.report.extractor;

public class ReportExtractorException extends RuntimeException {

    public ReportExtractorException(String message) {
        super(message);
    }

    public ReportExtractorException(String message, Throwable cause) {
        super(message, cause);
    }
}
